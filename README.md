# Freetrain
--------

Original README can be found in freetrain/doc/ along with the licence



## What I have learned so far:


Summary: the curent version does not work, SDL kinda works on OSX, but not Linux, and AgateLib version does not work.



This version of the game is impossible to port to linux without at least a rewrite using mono, .net core, or some libary, given that it releys on .net framework and windows graphics libaries. 

The "orginal" (the ones who translated it and started working on a linux port) creators were interested in porting the game to use Mono and libSDL to port the game or an obsure library called AgateLib in place of SDL, and there currently exists an attempted mono/sdl version and a mono/agate branch of the orginal.

The SVN links to the Orginal Branches are:

* http://freetrain.svn.sourceforge.net/svnroot/freetrain/branches/FreeTrainSDL
* http://freetrain.svn.sourceforge.net/svnroot/freetrain/branches/FreeTrainAgate


Currently, I have yet to build either or test them, but the SDL branch was unworking on a Debian and Fedora system in 2008.  The devs believed it was due to [this bug in mono.](https://bugzilla.novell.com/show_bug.cgi?id=374663) 
For the Agate system, the dev only commented: "It's not runnable yet. I will update these instructions if/when it becomes runnable."  


However, the game was able to run on MacOS using the Mono+SDL build.  Double-click on it to start. The dev stated For me, it hangs while loading plugins. I did get it to run once but it [unknown if it refers to the game or plugins] was abysmally slow.



